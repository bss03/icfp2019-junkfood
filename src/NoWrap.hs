{-# LANGUAGE DeriveDataTypeable, DeriveGeneric, TupleSections #-}

module NoWrap where

-- base
import Control.Applicative ((<|>))
import Control.Monad (guard)
import Data.Bits (Bits(unsafeShiftR))
import Data.Data (Data)
import Data.Functor.Const (Const(Const, getConst))
import Data.Functor.Identity (Identity(Identity))
import Data.Maybe (catMaybes, fromMaybe)
import Data.Typeable (Typeable)
import GHC.Generics (Generic)

-- hashable
import Data.Hashable (Hashable(hashWithSalt), Hashed, unhashed, mapHashed)

-- transformers
import Control.Monad.Trans.Class (MonadTrans(lift))
import Control.Monad.Trans.Reader (ReaderT, mapReaderT, Reader, ask, asks)

-- unordered-containers
import qualified Data.HashMap.Strict as HashMap
import Data.HashMap.Strict (HashMap)
import qualified Data.HashSet as HashSet
import Data.HashSet (HashSet)

import Data.Vector.Generic ((!), (//), accum)

-- icfp2019-junkfood
import NoBoost
import Solution
import Task hiding (BoosterCode(..))

data NoWrapEnv = NWEnv
  { nweMaxX :: Int
  , nweMaxY :: Int
  } deriving (Eq, Show, Data, Typeable, Generic)

data NoWrapState = NWSt
  { nwsPosition :: Point
  , nwsFastTurns :: Int
  , nwsDrillTurns :: Int
  , nwsFastCount :: Int
  , nwsDrillCount :: Int
  , nwsWalkable :: Hashed BitMap
  } deriving (Eq, Ord, Show, Typeable)

instance Hashable NoWrapState where
  hashWithSalt salt nws =
    salt `hashWithSalt` nwsPosition nws
         `hashWithSalt` nwsFastTurns nws
         `hashWithSalt` nwsDrillTurns nws
         `hashWithSalt` nwsFastCount nws
         `hashWithSalt` nwsDrillCount nws
         `hashWithSalt` nwsWalkable nws

lensNBSinNWS :: Functor f => (NoBoostState -> f NoBoostState) -> NoWrapState -> f NoWrapState
lensNBSinNWS f nws = fmap set $ f get
 where
  get = nwsPosition nws
  set nbs = nws { nwsPosition = nbs }

lensNBinNW :: Functor f => ((NoBoostEnv, NoBoostState) -> f (NoBoostEnv, NoBoostState))
           -> (NoWrapEnv, NoWrapState) -> f (NoWrapEnv, NoWrapState)
lensNBinNW f (nwe, nws) = fmap set $ f get
 where
  get = (nbe, nwsPosition nws)
   where
    nbe = NBEnv
      { nbeMaxX = nweMaxX nwe
      , nbeMaxY = nweMaxY nwe
      , nbeWalkable = nwsWalkable nws
      }
  set (nbe, nbs) = (newEnv, newSt)
   where
    newEnv = NWEnv
      { nweMaxX = nbeMaxX nbe
      , nweMaxY = nbeMaxY nbe
      }
    newSt = nws
      { nwsWalkable = nbeWalkable nbe
      , nwsPosition = nbs
      }

tickNWState :: NoWrapState -> NoWrapState
tickNWState nws = nws
  { nwsFastTurns = decNat $ nwsFastTurns nws
  , nwsDrillTurns = decNat $ nwsDrillTurns nws
  }
 where
  decNat n = max 0 $ n - 1

moveNWState :: Delta -> NoWrapState -> ReaderT NoWrapEnv Maybe NoWrapState
moveNWState d nws = do
  slow <- moveNWStateSlow d nws
  fast <- mapReaderT Just $ moveNWStateSlow d slow
  pure . tickNWState $ if 0 < nwsFastTurns nws then fromMaybe slow fast else slow

moveNWStateSlow :: Delta -> NoWrapState -> ReaderT NoWrapEnv Maybe NoWrapState
moveNWStateSlow d nws = do
  let newPos = addDelta (nwsPosition nws) d
  guard (0 <= x newPos && 0 <= y newPos)
  mx <- asks nweMaxX
  guard (x newPos < mx)
  my <- asks nweMaxY
  guard (y newPos < my)
  let
    step = do
      guard (unBitMap (unhashed $ nwsWalkable nws) ! x newPos ! y newPos)
      pure nws { nwsPosition = newPos }
    drill = do
      guard (0 < nwsDrillTurns nws)
      let makeWalkable (BitMap v) = BitMap $ accum (//) v [(x newPos, [(y newPos, True)])]
      pure nws
        { nwsPosition = newPos
        , nwsWalkable = mapHashed makeWalkable $ nwsWalkable nws
        }
  step <|> drill
 where

actionNWState :: Action -> NoWrapState -> ReaderT NoWrapEnv Maybe NoWrapState
actionNWState W nws = moveNWState Point{x=   0,y=   1} nws
actionNWState A nws = moveNWState Point{x=(-1),y=   0} nws
actionNWState S nws = moveNWState Point{x=   0,y=(-1)} nws
actionNWState D nws = moveNWState Point{x=   1,y=   0} nws
actionNWState F nws = do
  let fc = nwsFastCount nws
  guard (0 < fc)
  let ticked = tickNWState nws
  pure ticked
    { nwsFastCount = fc - 1
    , nwsFastTurns = nwsFastTurns ticked + 50
    }
actionNWState L nws = do
  let dc = nwsDrillCount nws
  guard (0 < dc)
  let ticked = tickNWState nws
  pure ticked
    { nwsDrillCount = dc - 1
    , nwsDrillTurns = nwsDrillTurns ticked + 30
    }
actionNWState _ _   = lift Nothing

projectNoBoost :: NoWrapState -> Reader NoWrapEnv (NoBoostEnv, NoBoostState)
projectNoBoost nws = do
  nwe <- ask
  pure . getConst $ lensNBinNW Const (nwe, nws)

graphNoWrap :: NoWrapState -> Reader NoWrapEnv (HashMap Action NoWrapState)
graphNoWrap nws = do
  adjList <- fmap catMaybes $ traverse gen [W,A,S,D,F,L]
  pure $ HashMap.fromList adjList
 where
  gen act = mapReaderT Identity . fmap (act,) $ actionNWState act nws

heuristicNW :: HashSet Point -> NoWrapState -> Reader NoWrapEnv Int
heuristicNW ps nws = do
  mx <- asks nweMaxX
  my <- asks nweMaxY
  pure . HashSet.foldl' min (halfUp mx + halfUp my) $ HashSet.map fastDist ps
 where
  halfUp n = (1 + n) `unsafeShiftR` 1
  pos = nwsPosition nws
  fastDist dest = halfUp (abs $ x dest - x pos) + halfUp (abs $ y dest - y pos)

goalSetNW :: HashSet Point -> NoWrapState -> Bool
goalSetNW ps = getConst . lensNBSinNWS (Const . goalSetNB ps)

goalBitmapNW :: BitMap -> NoWrapState -> Bool
goalBitmapNW bm = getConst . lensNBSinNWS (Const . goalBitmapNB bm)
