{-# LANGUAGE DeriveDataTypeable #-}
module Caching
  (Caching, mkCache, cached, original, calculation, update, updatePoly, cacheLens, mapCache)
where

-- base
import Data.Functor.Classes
  ( Eq2(liftEq2), Eq1(liftEq), eq2, Ord1(liftCompare), Ord2(liftCompare2), compare1
  , Show2(liftShowsPrec2), Show1(liftShowsPrec), showsPrec2, showsBinaryWith
  )
import Data.Functor.Const (Const(Const, getConst))
import Data.Functor.Identity (Identity(Identity, runIdentity))
import Data.Typeable (Typeable)

-- hashable
import Data.Hashable (Hashable(hashWithSalt), hashUsing)
import Data.Hashable.Lifted (Hashable1(liftHashWithSalt), Hashable2(liftHashWithSalt2))

data Caching c a = Caching c !a (a -> c) deriving (Typeable)

mkCache :: (a -> c) -> a -> Caching c a
mkCache calc target = Caching (calc target) target calc

cached :: Caching c a -> c
cached (Caching c _ _) = c

cacheLens :: Functor f => (a -> f b) -> (b -> a) -> Caching c a -> f (Caching c b)
cacheLens up proj (Caching _ x f) = fmap (mkCache (f . proj)) $ up x

original :: Caching c a -> a
original = getConst . cacheLens Const id

calculation :: Caching c a -> a -> c
calculation (Caching _ _ f) = f

instance Eq2 Caching where
  liftEq2 eqC eqA c d = eqC (cached c) (cached d) && eqA (original c) (original d)

instance Eq c => Eq1 (Caching c) where
  liftEq = liftEq2 (==)

instance (Eq c, Eq a) => Eq (Caching c a) where
  (==) = eq2

instance Eq c => Ord1 (Caching c) where
  liftCompare compareA c d = compareA (original c) (original d)

instance Ord2 Caching where
  liftCompare2 _ compareA c d = compareA (original c) (original d)

instance (Eq c, Ord a) => Ord (Caching c a) where
  compare = compare1

instance Show2 Caching where
  liftShowsPrec2 spC _ spA _ p c = showsBinaryWith spC spA "Caching" p (cached c) (original c)

instance Show c => Show1 (Caching c) where
  liftShowsPrec = liftShowsPrec2 showsPrec showList

instance (Show c, Show a) => Show (Caching c a) where
  showsPrec = showsPrec2

instance Foldable (Caching c) where
  foldMap f = f . original

instance Hashable1 (Caching c) where
  liftHashWithSalt hwsA salt = hwsA salt . original

instance Hashable2 Caching where
  liftHashWithSalt2 _ = liftHashWithSalt

instance Hashable a => Hashable (Caching c a) where
  hashWithSalt = hashUsing original

updatePoly :: (a -> b) -> (b -> a) -> Caching c a -> Caching c b
updatePoly up proj = runIdentity . cacheLens (Identity . up) proj

update :: (a -> a) -> Caching c a -> Caching c a
update up = updatePoly up id

mapCache :: (c -> d) -> Caching c a -> Caching d a
mapCache f (Caching cv x cf) = Caching (f cv) x (f . cf)
