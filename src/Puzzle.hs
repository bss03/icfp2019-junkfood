{-# LANGUAGE DeriveDataTypeable, DeriveGeneric #-}
module Puzzle where

-- base
import GHC.Generics (Generic)
import Data.Data (Data)
import Data.Functor (void)
import Data.List (minimumBy, maximumBy, sortOn)
import qualified Data.List as L

-- bytestring
import qualified Data.ByteString.Lazy as Lazy
import Data.ByteString.Builder (Builder)

-- megaparsec
import Text.Megaparsec (Parsec, sepBy, single, some)
import Text.Megaparsec.Byte (digitChar)

-- unordered-conatainers
import Data.HashMap.Strict (HashMap)
import qualified Data.HashMap.Strict as HashMap

-- local imports
import Task hiding (maxX, maxY)

data Puzzle = Puzzle
  { bNum  :: Int -- ^ block number
  , epoch :: Int -- ^ complexity measure
  , tSize :: Int -- ^ maxX or maxY should be > 90% of tSize
  , vMin  :: Int -- ^ minimum number of verticies
  , vMax  :: Int -- ^ maximum number of verticies
  , mNum  :: Int -- ^ number of manipulator Boosters to place
  , fNum  :: Int -- ^ number of speed Boosters to place
  , dNum  :: Int -- ^ number of drill Boosters to place
  , rNum  :: Int -- ^ number of teleport Boosters to place
  , cNum  :: Int -- ^ number of cloning Boosters to place
  , xNum  :: Int -- ^ number of spawning locations to place
  , insideSquares :: [Point]
  , outsideSquares :: [Point]
  } deriving (Eq, Ord, Read, Data, Generic, Show)

data MapItem = ManipItem
             | SpeedItem
             | DrillItem
             | TeleportItem
             | CloneItem
             | SpawnPoint
  deriving (Eq, Ord, Enum, Read, Data, Generic, Show)

data PuzzleSolution = PuzzleSolution
  { puzzle   :: Puzzle
  , vertexes :: Map
  , bMap     :: ([MapItem], HashMap Point MapItem)
  , start    :: Point
  } deriving (Eq, Ord, Read, Data, Generic, Show)

mkSolution :: Puzzle -> PuzzleSolution
mkSolution p = let ps = PuzzleSolution { puzzle = p
                                       , vertexes = bm
                                       , bMap = distBoosters p
                                       , start = placeStart bm (snd $ distBoosters p)
                                       } in
  if null . fst $ bMap ps
    then -- All MapItems have been distributed
      ps
    else -- Distribute the rest of the MapItems
      placeRemainingBoosts ps
  where
    bm = boundaryMap p

data Tile = Unknown
          | Inside
          | Outside
  deriving (Eq, Ord, Enum, Read, Data, Generic, Show)

gatherInsideBorderTiles :: HashMap Point Tile -> [Point]
gatherInsideBorderTiles bag = -- Get minY for every x, maxX for every y, maxY for every x in reverse, minX for every Y in reverse
       L.foldr (\v acc -> acc ++ [Point{x=v,y= y .
           L.minimumBy compareY . L.map fst . HashMap.toList
           $ HashMap.filterWithKey (\k _ ->
               v == x k
             ) bag }]
       ) [] [minX..maxX]
    ++ L.foldr (\v acc -> acc ++ [Point{y=v,x= x .
           L.maximumBy compareX . L.map fst . HashMap.toList
           $ HashMap.filterWithKey (\k _ ->
               v == y k
             ) bag }]
       ) [] [minY..maxY]
    ++ L.foldr (\v acc -> acc ++ [Point{x=v,y= y .
           L.maximumBy compareY . L.map fst . HashMap.toList
           $ HashMap.filterWithKey (\k _ ->
               v == x k
             ) bag }]
       ) [] [maxX..minX]
    ++ L.foldr (\v acc -> acc ++ [Point{y=v,x= x .
           L.minimumBy compareX . L.map fst . HashMap.toList
           $ HashMap.filterWithKey (\k _ ->
               v == y k
             ) bag }]
       ) [] [maxY..minY]

  where
    inTiles = HashMap.filter (== Inside) bag
    theList = L.map fst $ HashMap.toList inTiles
    minX = x $ L.minimumBy compareX theList
    minY = y $ L.minimumBy compareY theList
    maxX = x $ L.maximumBy compareX theList
    maxY = y $ L.maximumBy compareY theList

boundaryMap :: Puzzle -> Map
boundaryMap puzz = gatherInsideBorderTiles . HashMap.filter (== Inside) $ growWalls . HashMap.union outsideTiles . polyFill $ bboxInside minPoint maxPoint
  where
    iSqs = insideSquares puzz
    oSqs = outsideSquares puzz

    bboxInside  :: Point -> Point -> HashMap Point Tile
    bboxOutside :: Point -> Point -> HashMap Point Tile
    bboxInside  ll ur = HashMap.fromList [(Point{x=a,y=b}, Inside)  | a <- [x ll..x ur], b <- [y ll..y ur] ]
    bboxOutside ll ur = HashMap.fromList [(Point{x=a,y=b}, Outside) | a <- [x ll..x ur], b <- [y ll..y ur] ]

    growWalls :: HashMap Point Tile -> HashMap Point Tile
    growWalls tiles  = final . third $ second first
      where
        first    = fillDodging tiles insideTiles minPoint ll
        second t = fillDodging t insideTiles lMidP lr
        third  t = fillDodging t insideTiles uMidP ul
        final  t = fillDodging t insideTiles Point{x=0,y=maximumY} ur
        uMidP = Point{x=(maximumX - minimumX `div` 2),y=(maximumY - minimumY `div` 2)}
        lMidP = Point{x=(maximumX - minimumX `div` 2),y=0}
        ll = rangeQuery tiles
           minimumX
           minimumY
          (maximumX - minimumX `div` 2)
          (maximumY - minimumY `div` 2)
           Outside
        lr = rangeQuery tiles
          (maximumX - minimumX `div` 2)
           minimumY
           maximumX
          (maximumY - minimumY `div` 2)
           Outside
        ul =  rangeQuery tiles
           minimumX
          (maximumY - minimumY `div` 2)
          (maximumX - minimumX `div` 2)
           maximumY
           Outside
        ur =  rangeQuery tiles
          (maximumX - minimumX `div` 2)
          (maximumY - minimumY `div` 2)
           maximumX
           maximumY
           Outside

    fillDodging :: HashMap Point Tile -> HashMap Point Tile -> Point -> HashMap Point Tile -> HashMap Point Tile
    fillDodging bag noFill startPos targets = HashMap.foldrWithKey (\k _v _acc ->
          let jump = jumpX startPos k in
            let line = strideY jump k in
              let bs = sortOn (yDist jump) $ blockers noFill line in
                if null bs
                  then foldr (\p acc -> fillAsOutside acc p) bag line
                  else fst $ foldr (\p (b,s) -> dodge s p b) (bag,jump) bs
        ) bag targets
      where
        dodge :: Point -> Point -> HashMap Point Tile -> (HashMap Point Tile, Point)
        dodge s p abag = let line = init (strideY s p) in
            let jump = jumpY s p in
              case compareY s p of
                LT -> -- below the target
                    (foldr (flip fillAsOutside) abag $ line ++ [ moveUp jump
                                                                         , moveRight jump
                                                                         , moveUp (moveRight jump)
                                                                         , moveDown (moveRight jump)
                                                                         ], moveUp jump)
                GT -> -- above the target
                    (foldr (flip fillAsOutside) abag $ line ++ [ moveDown jump
                                                                         , moveRight jump
                                                                         , moveUp (moveRight jump)
                                                                         , moveDown (moveRight jump)
                                                                         ], moveDown jump)
                EQ -> -- We are done as we need to dodge a starting point
                    (abag, p)
        blockers :: HashMap Point Tile -> [Point] -> [Point]
        blockers miss ps = foldr (\p acc -> if HashMap.member p miss then acc ++ [p] else acc) [] ps

        moveRight p = addDelta Point{x=1,y=0} p
        moveUp    p = addDelta Point{x=0,y=1} p
        moveDown  p = addDelta Point{x=0,y= -1} p
        yDist p q = abs (y p - y q)

        jumpX p g = Point{x=x g, y=y p}
        jumpY p g = Point{x=x p, y=y g}
        strideY p g = [Point{x=x p,y=a} | a <- [y p..y g]]

        fillAsOutside b p = HashMap.adjust (\_ -> Outside) p b

    rangeQuery :: HashMap Point Tile -> Int -> Int -> Int -> Int -> Tile -> HashMap Point Tile
    rangeQuery bag ax ay bx by tile =
        HashMap.filterWithKey (\(Point kx ky) v ->
          (v == tile && ((ax <= kx) && (kx <= bx))
                     && (((ay <= ky) && (ky <= by)))
          )) bag

    insideTiles :: HashMap Point Tile
    insideTiles = HashMap.fromList [(p, Inside) | p <- iSqs]

    outsideTiles :: HashMap Point Tile
    outsideTiles = HashMap.fromList [(p, Outside) | p <- oSqs]

    minPoint = Point{x=minimumX,y=minimumY}
    maxPoint = Point{x=maximumX,y=maximumY}
    minimumX = x $ minimumBy compareX iSqs
    minimumY = y $ minimumBy compareY iSqs
    maximumX = x $ maximumBy compareX iSqs
    maximumY = y $ maximumBy compareY iSqs

    polyFill :: HashMap Point Tile -> HashMap Point Tile
    polyFill inside =
      if minPoint == Point{x=0,y=0}
        then inside -- Our bounding box is the whole field
        else -- We have to add Outside tiles to the set from (0,0) to (maxX,minY) and (0,0 to (minX,maxY)
          HashMap.unions [ inside
                         , bboxOutside Point{x=0,y=0} Point{x=maximumX,y=minimumY}
                         , bboxOutside Point{x=0,y=0} Point{x=minimumX,y=maximumY}
                         ]

placeStart :: [Point] -> HashMap Point MapItem -> Point
placeStart choices invalid = head possibilities
  where
    possibilities = dropWhile (\a -> case HashMap.lookup a invalid of
                                  Just _ -> True
                                  Nothing -> False
                              ) choices

placeRemainingBoosts :: PuzzleSolution -> PuzzleSolution
placeRemainingBoosts _ps = _ps

distBoosters :: Puzzle -> ([MapItem], HashMap Point MapItem)
distBoosters p = let bs = mkBoosters p in
  if (length $ insideSquares p) <= length bs
      then ([], HashMap.fromList $ zip (insideSquares p) bs)
      else (bs, HashMap.empty)

mkBoosters :: Puzzle -> [MapItem] 
mkBoosters p = replicate (mNum p) ManipItem
            ++ replicate (fNum p) SpeedItem
            ++ replicate (dNum p) DrillItem
            ++ replicate (rNum p) TeleportItem
            ++ replicate (cNum p) CloneItem
            ++ replicate (xNum p) SpawnPoint

puzzleParser :: Ord e => Parsec e Lazy.ByteString Puzzle
puzzleParser = do
    numbers <- numsParser
    hash
    isqs <- sqsParser
    hash
    osqs <- sqsParser
    if length numbers == 11
      then case numbers of
        (a:b:c:d:e:f:g:h:i:j:k:[]) -> 
          return Puzzle { bNum = a
                        , epoch = b
                        , tSize = c
                        , vMin = d
                        , vMax = e
                        , mNum = f
                        , fNum = g
                        , dNum = h
                        , rNum = i
                        , cNum = j
                        , xNum = k
                        , insideSquares = isqs
                        , outsideSquares = osqs
                        }

        _ -> error "Numbers parsed for puzzle were not == 11 before the #"
      else error "Numbers parsed for puzzle were not == 11 before the #"
  where
    hash = punct '#'
    repSep parser sepChar = sepBy parser $ punct sepChar
    natParser = fmap (read . fmap fromAscii) $ some digitChar
    numsParser = repSep natParser ','
    sqsParser = repSep pointParser ','
    punct = void . single . ascii
    pointParser = do
      punct '('
      ix <- natParser
      punct ','
      iy <- natParser
      punct ')'
      return Point { x = ix, y = iy }

puzzleBuilder :: PuzzleSolution -> Builder
puzzleBuilder = undefined

{- Calculate the Manhattan distance between any two points -}
distance :: Point -> Point -> Int
distance (Point ax ay) (Point bx by) = abs (ax - bx) +  abs (ay - by)

compareX :: Point -> Point -> Ordering
compareY :: Point -> Point -> Ordering
compareX (Point ax _) (Point bx _) = compare ax bx
compareY (Point _ ay) (Point _ by) = compare ay by
