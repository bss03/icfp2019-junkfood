{-# LANGUAGE TupleSections #-}

{-|data
Module      : MineMap
Description : Types for encding Mine Maps
Copyright   : (c) Zac Slade, 2019
License     : AllRightsReserved
Maintainer  : krakrjak@gmail.com
Stability   : experimental

Definitions for the data types needed to encode map states for the ICFP 2019
Programming competition.
-}

module MineMap where

-- base
import Control.Applicative ((<|>))
import Control.Monad ((>=>), guard)
import Data.Functor.Compose (Compose(Compose, getCompose))
import Data.Maybe (fromMaybe, mapMaybe)
import Prelude hiding (any, concat)

-- containers
import qualified Data.IntMap as IM

-- hashable
import Data.Hashable (Hashable(hashWithSalt), hashUsing, Hashed, hashed, unhashed, mapHashed)

-- unordered-containers
import Data.HashMap.Strict (HashMap)
import qualified Data.HashMap.Strict as HashMap

-- vector
import Data.Vector (Vector, any)
import qualified Data.Vector.Unboxed as Unboxed
import qualified Data.Vector.Generic as Vector
import Data.Vector.Generic (accum, (//), (!))

-- Internal imports
import Solution (Action(..))
import qualified Task (BoosterCode(..))
import Task (Point(..), Delta, addDelta, Task(..), TaskEx(..))

data Boost = Drill
           | Speed
           | Clone
           | Manipulator
  deriving (Eq, Ord, Enum, Show)

instance Hashable Boost where
  hashWithSalt = hashUsing fromEnum

data CellState = Rock -- ^ Can penetrate with a Drill only
               | Unpainted -- ^ Still left to visit
               | Painted -- ^ Been visited
  deriving (Eq, Ord, Enum, Show)

data MineMap = MineMap
  { needsWrap :: Vector (Unboxed.Vector Bool)
  , boostLocs :: HashMap Point Boost
  , walkable  :: Vector (Unboxed.Vector Bool)
  , maximumX  :: Int
  , maximumY  :: Int
  } deriving (Eq, Ord, Show)

instance Hashable MineMap where
  hashWithSalt salt mm =
    salt `hashMapBitmapWithSalt` walkable mm
         `hashMapBitmapWithSalt` needsWrap mm
         `hashWithSalt` boostLocs mm
         `hashWithSalt` maximumX mm
         `hashWithSalt` maximumY mm
   where
    hashMapBitmapWithSalt = Vector.foldl' hashVectorWithSalt
    hashVectorWithSalt = Vector.foldl' hashWithSalt

data MinerState = MinerState
  { minerPos      :: Point   -- ^ where are we?
  , manipDeltas   :: [Delta] -- ^ Along with the Manipulators, what area do we cover?
  , boostState    :: BoostedState -- ^ What boosters are in effect and how many do we have?
  , mapState      :: Hashed MineMap -- ^ What's the current lay of the land
  } deriving (Eq, Ord, Show)

instance Hashable MinerState where
  hashWithSalt salt st =
    salt `hashWithSalt` mapState st
         `hashWithSalt` minerPos st
         `hashWithSalt` manipDeltas st
         `hashWithSalt` boostState st

data BoostedState = BoostedState
  { speedTurns :: Int -- ^ Turns left for speed boost. 0 when not activated
  , drillTurns :: Int -- ^ Turns left for drill boost. 0 when not activated
  , drillCount :: Int -- ^ How many drills do we have?
  , speedCount :: Int -- ^ How many speed boosts can we spend?
  , manipCount :: Int -- ^ How many manipulators can we deploy?
  , cloneCount :: Int -- ^ How many clone boosts are available?
  } deriving (Eq, Ord, Show)

instance Hashable BoostedState where
  hashWithSalt salt bs =
    salt `hashWithSalt` speedTurns bs
         `hashWithSalt` drillTurns bs
         `hashWithSalt` drillCount bs
         `hashWithSalt` speedCount bs
         `hashWithSalt` manipCount bs
         `hashWithSalt` cloneCount bs

fromTaskEx :: TaskEx -> MinerState
fromTaskEx tex = initialState
 where
  t = task tex
  codeToBoost Task.B = Just Manipulator
  codeToBoost Task.F = Just Speed
  codeToBoost Task.L = Just Drill
  codeToBoost Task.X = Nothing
  codeToBoost Task.R = Nothing
  codeToBoost Task.C = Just Clone
  taskBoostToMapBoost code loc = fmap (loc,) $ codeToBoost code
  initialWalk = canWalk tex
  initialPos = initialLocation t
  initialManips = [Point{x=1,y=0},Point{x=1,y=1},Point{x=1,y=(-1)}]
  initialMap = MineMap
    { walkable = initialWalk
    , needsWrap = applyWrap initialPos initialManips initialWalk
    , boostLocs = HashMap.fromList . mapMaybe (uncurry taskBoostToMapBoost) $ boosters t
    , maximumX = maxX tex
    , maximumY = maxY tex
    }
  initialState = MinerState
    { mapState = hashed initialMap
    , minerPos = initialPos
    , manipDeltas = initialManips
    , boostState = BoostedState
      { speedTurns = 0
      , drillTurns = 0
      , drillCount = 0
      , speedCount = 0
      , manipCount = 0
      , cloneCount = 0
      }
    }

-- | Consume running boosts for the tick (add other things that are action-independent, probably.)
tickState :: MinerState -> MinerState
tickState st = st { boostState = tickBoostState $ boostState st }

-- | Consume running boosts for the tick
tickBoostState :: BoostedState -> BoostedState
tickBoostState bs = bs
  { speedTurns = max 0 $ speedTurns bs - 1
  , drillTurns = max 0 $ drillTurns bs - 1
  }

{-|
Pick up any boosts that are on the current square, transferring them from the MineMap to the
BoostedState.
-}
pickupState :: MinerState -> MinerState
pickupState st =
  case getCompose ldp of
   Nothing -> st
   Just (boostHere, boostRemain) -> st
     { mapState = mapHashed (\ms -> ms { boostLocs = boostRemain }) (mapState st)
     , boostState = pickupBoostState boostHere $ boostState st
     }
 where
  ldp = HashMap.alterF (Compose . fmap (, Nothing))
                       (minerPos st)
                       (boostLocs . unhashed $ mapState st)

pickupBoostState :: Boost -> BoostedState -> BoostedState
pickupBoostState Drill       bs = bs { drillCount = 1 + drillCount bs }
pickupBoostState Speed       bs = bs { speedCount = 1 + speedCount bs }
pickupBoostState Clone       bs = bs { cloneCount = 1 + cloneCount bs }
pickupBoostState Manipulator bs = bs { manipCount = 1 + manipCount bs }

{-|
Advance the state baed on the simple movement actions.  Position will be updated by the given
delta, boosts will be acquired (or walls drilled), parts of the mine will be wrapped, if fast
wheels are active, those steps will be repeated, finally we advance time.
-}
moveState :: Delta -> MinerState -> Maybe MinerState
moveState d st = do
  slow <- moveStateSlowNoTick d st
  let fast = moveStateSlowNoTick d slow
  return . tickState $ if 0 < speedTurns (boostState st) then fromMaybe slow fast else slow

moveStateSlowNoTick :: Delta -> MinerState -> Maybe MinerState
moveStateSlowNoTick d st = do
  let newPos = addDelta (minerPos st) d
  guard (0 <= x newPos && x newPos < maximumX ms
         && 0 <= y newPos && y newPos < maximumY ms)
  let
    step = do
      guard (walkable ms ! x newPos ! y newPos)
      return $ pickupState st { minerPos = newPos }
    drill = do
      guard (0 < drillTurns (boostState st))
      let w = walkable ms
      return st
        { minerPos = newPos
        , mapState = hashed ms { walkable = accum (//) w [(x newPos, [(y newPos, True)])] }
        }
  fmap applyWrapState $ step <|> drill
 where
  ms = unhashed $ mapState st

-- | Mark as wrapped the current miner position and maniplutors.
applyWrapState :: MinerState -> MinerState
applyWrapState st = st
  { mapState = mapHashed (\ims -> ims
    { needsWrap = applyWrap (minerPos st) (manipDeltas st) (needsWrap ims)
    }) ms
  }
 where
  ms = mapState st

actionState :: Action -> MinerState -> Maybe MinerState
actionState W st = moveState Point{x=   0,y=   1} st
actionState A st = moveState Point{x=(-1),y=   0} st
actionState S st = moveState Point{x=   0,y=(-1)} st
actionState D st = moveState Point{x=   1,y=   0} st
actionState F st = do
  let
    bs = boostState st
    sc = speedCount bs
  guard (0 < sc)
  let
    ticked = tickState st
    tbs = boostState ticked
  return ticked
    { boostState = tbs
      { speedTurns = speedTurns tbs + 50
      , speedCount = sc - 1
      }
    }
actionState L st = do
  let
    bs = boostState st
    dc = drillCount bs
  guard (0 < dc)
  let
    ticked = tickState st
    tbs = boostState ticked
  return ticked
    { boostState = tbs
      { drillTurns = drillTurns tbs + 30
      , drillCount = dc - 1
      }
    }
actionState _ _ = Nothing

-- Multiple TURNS, NOT multiple bots.
actionsState :: [Action] -> MinerState -> Maybe MinerState
actionsState = foldr ((>=>) . actionState) pure

{-| What boosts are currently activated? For this miner -}
boostActive :: BoostedState -> [Boost]
boostActive bstate = [Speed | 0 < speedTurns bstate]
                  ++ [Drill | 0 < drillTurns bstate]

{-| What boosts are currently Available? For this miner -}
boostAvailable :: BoostedState -> [Boost]
boostAvailable bstate = [Speed | 0 < speedCount bstate]
                     ++ [Drill | 0 < drillCount bstate]
                     ++ [Clone | 0 < cloneCount bstate]
                     ++ [Manipulator | 0 < manipCount bstate]

{-|
Substract one point ("source" / "origin") from another ("destination").  The result is how to move
from the second point to the first point.

For arbitrary x and y, addDelta (substractPoints y x) x = y
-}
subtractPoints :: Point -> Point -> Delta
subtractPoints Point{x=dx,y=dy} Point{x=sx,y=sy} = Point{x=dx-sx,y=dy-sy}

-- | Rotate a delta clockwise around its origin
rotateCW :: Delta -> Delta
rotateCW d = Point { x = y d, y = negate $ x d }

-- | Rotate a delta counter-clockwise around its origin
rotateCCW :: Delta -> Delta
rotateCCW d = Point { x = negate $ y d, y = x d}

{-|
Update a "needsWrapped" vector of vectors, based on the current worker position and the delta to
all of its manipulators.  TODO: DOES NOT CHECK REACHABILITY. Each nested vector is bulk-updated at
most once, and the outer vector is accumulated at most once.
-}
applyWrap ::  Point -> [Delta] -> Vector (Unboxed.Vector Bool) -> Vector (Unboxed.Vector Bool)
applyWrap pos ds v = accum (\uv ys -> uv // fmap (, False) ys) v updates
 where
  mx = length v
  my = Vector.length (v ! 0)
  inBoundingBox p = 0 <= x p && x p < mx && 0 <= y p && y p < my
  updates = IM.toList . IM.fromListWith (++)
          . fmap (\p -> (x p, [y p])) . filter inBoundingBox $ pos : fmap (addDelta pos) ds

countUnwrapped :: Vector (Unboxed.Vector Bool) -> Int
countUnwrapped = sum . fmap (Vector.sum . Vector.map fromEnum)

anyUnwrapped :: Vector (Unboxed.Vector Bool) -> Bool
anyUnwrapped = any Vector.or

{-|
Second argument should include not only extension boosters in inventory, but also all extension
boosts yet to be picked up, to avoid overestimating.
-}
maxRemainingTime :: [Delta] -> Int -> Vector (Unboxed.Vector Bool) -> Int
maxRemainingTime ds extBoosts needWrap =
  minimum $ fmap (\eb -> remainUnwrapped `div` (currentMaxWrap + eb)) [0..extBoosts]
 where
  currentMaxWrap = 1 + length ds
  remainUnwrapped = countUnwrapped needWrap
