{-# LANGUAGE DeriveDataTypeable, DeriveGeneric #-}
module Task
where

-- base
import Control.Monad (forM_, when)
import Data.Bits (xor)
import Data.Char (chr, ord)
import Data.Data (Data)
import Data.Functor (void)
import Data.List.NonEmpty (NonEmpty((:|)))
import Data.Typeable (Typeable)
import Data.Void (Void)
import Data.Word (Word8)
import GHC.Generics (Generic)
import Prelude hiding (map, scanl1)

-- bytestring
import qualified Data.ByteString.Lazy as LBS
import qualified Data.ByteString.Lazy as Lazy

-- hashable
import Data.Hashable (Hashable)

-- megaparsec
import Text.Megaparsec
  ( Parsec, ErrorItem(Tokens)
  , anySingle, errorBundlePretty, parse, sepBy, single, some, try, unexpected
  )
import Text.Megaparsec.Byte (digitChar)

-- vector
import Data.Vector (Vector, generate)
import qualified Data.Vector.Unboxed as Unboxed (Vector)
import Data.Vector.Unboxed (create, slice, scanl1)
import Data.Vector.Unboxed.Mutable (new, write)

data Task = Task
  { map :: Map
  , initialLocation :: Point
  , obstacles :: [Map]
  , boosters :: [(BoosterCode, Point)]
  } deriving (Eq, Show, Read, Data, Typeable, Generic)

type Map = [Point]

data Point = Point
  { x :: Int
  , y :: Int
  } deriving (Eq, Ord, Show, Read, Data, Typeable, Generic)
type Delta = Point

-- | Result is input point shifted by the input delta
addDelta :: Delta -> Point -> Point
addDelta Point{x=dx,y=dy} Point{x=sx,y=sy} = Point{x=sx+dx,y=sy+dy}


instance Hashable Point where

data BoosterCode = B | F | L | X | R | C
  deriving (Eq, Ord, Show, Read, Data, Typeable, Generic)

-- If input is an ASCII char, the byte used to represent it, otherwise undefined.
ascii :: Char -> Word8
ascii = fromIntegral . ord

fromAscii :: Word8 -> Char
fromAscii = chr . fromIntegral

taskParser :: Parsec Void Lazy.ByteString Task
taskParser = do
  tmap <- mapParser
  hash
  initialLoc <- pointParser
  hash
  obs <- repSep mapParser ';'
  hash
  boosts <- repSep boosterParser ';'
  return Task
    { map = tmap
    , initialLocation = initialLoc
    , obstacles = obs
    , boosters = boosts
    }
 where
  mapParser = repSep pointParser ','
  hash = punct '#'
  pointParser = do
    punct '('
    ix <- natParser
    punct ','
    iy <- natParser
    punct ')'
    return Point { x = ix, y = iy }
  boosterParser = do
    code <- try $ do
      codeByte <- anySingle
      case fromAscii codeByte of
       'B' -> return B
       'F' -> return F
       'L' -> return L
       'X' -> return X
       'R' -> return R
       'C' -> return C
       _   -> unexpected . Tokens $ codeByte :| []
    loc <- pointParser
    return (code, loc)
  repSep parser sepChar = sepBy parser $ punct sepChar
  punct = void . single . ascii
  natParser = fmap (read . fmap fromAscii) $ some digitChar

-- | WARNING: Partial, calls error on parse failure, but useful in GHCi
fromFile :: String -> IO Task
fromFile filename = do
  taskData <- LBS.readFile filename
  case parse taskParser filename taskData of
   Left eb -> error $ errorBundlePretty eb
   Right t -> return t

data TaskEx = TaskEx
  { task :: Task
  , maxX :: Int
  , maxY :: Int
  , canWalk :: Vector (Unboxed.Vector Bool)
  } deriving (Eq, Show, Read, Data, Typeable, Generic)

polySegs :: [Point] -> [(Point, Point)]
polySegs poly = zip poly $ drop 1 poly ++ [head poly]

extendTask :: Task -> TaskEx
extendTask t = TaskEx
  { task = t
  , maxX = mx
  , maxY = my
  , canWalk = walkable
  }
 where
  mx = maximum . fmap x $ map t
  my = maximum . fmap y $ map t
  walkable = fmap (scanl1 xor) transition
  transition = generate mx $ \ix -> slice (ix * my) my packTransition
   where
    packTransition = create $ do
      v <- new (mx * my) -- each bottom edge of cells
      let pairs = concat $ polySegs (map t) : fmap polySegs (obstacles t)
      forM_ pairs $ \(Point { x = sx, y = sy }, Point { x = dx, y = dy }) ->
        when (sy == dy && sy < my) $ do -- horizonal segments not on edge
          forM_ [(min sx dx)..((max sx dx) - 1)] $ \ix ->
            write v (ix * my + sy) True -- mark as vertical transision
      return v
