{-# LANGUAGE DeriveDataTypeable, DeriveGeneric #-}
module Solution
where

-- base
import Data.Data (Data)
import Data.Foldable (fold)
import Data.List.NonEmpty (NonEmpty, intersperse)
import Data.Typeable (Typeable)
import GHC.Generics (Generic)

-- bytestring
import Data.ByteString.Builder (Builder, char7, intDec, string7)

-- hashable
import Data.Hashable (Hashable(hashWithSalt))

type Solution = [Actions]
type Actions = NonEmpty Action

data Action = W -- Move Up
            | S -- Move Down
            | A -- Move Left
            | D -- Move Right
            | Z -- Do Nothing
            | E -- Turn 90 deg. CW
            | Q -- Turn 90 deg. CCW
            | B Int Int -- Attach manipulator
            | F -- Use Fast Wheels
            | L -- Use Drill
            | R -- Install Reset Beacon
            | T Int Int -- Teleport to (previously installed) beacon
            | C -- Clone
            deriving (Eq, Ord, Show, Read, Data, Typeable, Generic)

instance Hashable Action where
  hashWithSalt s W = hashWithSalt s (0::Int)
  hashWithSalt s S = hashWithSalt s (1::Int)
  hashWithSalt s A = hashWithSalt s (2::Int)
  hashWithSalt s D = hashWithSalt s (3::Int)
  hashWithSalt s Z = hashWithSalt s (4::Int)
  hashWithSalt s E = hashWithSalt s (5::Int)
  hashWithSalt s Q = hashWithSalt s (6::Int)
  hashWithSalt s (B dx dy) = hashWithSalt s (7::Int) `hashWithSalt` dx `hashWithSalt` dy
  hashWithSalt s F = hashWithSalt s (8::Int)
  hashWithSalt s L = hashWithSalt s (9::Int)
  hashWithSalt s R = hashWithSalt s (10::Int)
  hashWithSalt s (T x y) = hashWithSalt s (11::Int) `hashWithSalt` x `hashWithSalt` y
  hashWithSalt s C = hashWithSalt s (12::Int)

solutionBuilder :: Solution -> Builder
solutionBuilder = foldMap actionsBuilder
 where
  actionsBuilder = fold . intersperse (char7 '#') . fmap actionBuilder
  actionBuilder W = char7 'W'
  actionBuilder S = char7 'S'
  actionBuilder A = char7 'A'
  actionBuilder D = char7 'D'
  actionBuilder Z = char7 'Z'
  actionBuilder E = char7 'E'
  actionBuilder Q = char7 'Q'
  actionBuilder (B dx dy) = string7 "B(" <> intDec dx <> char7 ',' <> intDec dy <> char7 ')'
  actionBuilder F = char7 'F'
  actionBuilder L = char7 'L'
  actionBuilder R = char7 'R'
  actionBuilder (T x y) = string7 "T(" <> intDec x <> char7 ',' <> intDec y <> char7 ')'
  actionBuilder C = char7 'C'
