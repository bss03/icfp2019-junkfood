-- Taken from https://raw.githubusercontent.com/weissi/astar/master/src/Data/Graph/AStar.hs
-- Modified to use HashPSQ instead of OrdPSQ
-- Modified to store Path as edge labels in PET object
-- Dropped distance function, which was always 1 for us.
-- Dropped heuristic, because we couldn't come up with a good one
-- (Stopped being A* here; now just a BFS)
-- Dropped score, since it is just the priority without the hueristic, and we store it in PET
module AStar (aStar,aStarM) where

import qualified Data.HashSet as Set
import Data.HashSet (HashSet)
import Data.Hashable (Hashable(..))
import qualified Data.HashMap.Strict as Map
import Data.HashMap.Strict (HashMap)
import qualified Data.HashPSQ as PSQ
import Data.HashPSQ (HashPSQ, minView)
import Data.List (foldl')
import Control.Monad (foldM)

data PathEdgesTagged l n = PET
  { revPath :: ![l]
  , pathLen :: !Int
  , node :: !n
  } deriving (Show)
instance Eq n => Eq (PathEdgesTagged l n) where
  PET{node=n} == PET{node=m} = n == m
instance Ord n => Ord (PathEdgesTagged l n) where
  compare PET{node=n} PET{node=m} = compare n m
instance Hashable n => Hashable (PathEdgesTagged l n) where
  hashWithSalt salt PET{node=n} = hashWithSalt salt n

data AStar a el = AStar { visited  :: !(HashSet a),
                          waiting  :: !(HashPSQ (PathEdgesTagged el a) Int ()) }
    deriving Show

aStarInit :: (Hashable a, Ord a) => a -> AStar a el
aStarInit start = AStar { visited  = Set.empty,
                          waiting  = PSQ.singleton PET{revPath=[],pathLen=0,node=start} 0 () }

runAStar :: (Hashable a, Ord a, Hashable el)
         => (a -> HashMap el a)          -- outgoing edges (label -> destination) from source in graph
         -> (a -> Bool)                  -- goal
         -> a                            -- starting vertex
         -> Maybe (PathEdgesTagged el a) -- final state

runAStar graph goal start = aStar' (aStarInit start)
  where aStar' s
          = case minView (waiting s) of
              Nothing            -> Nothing
              Just (tx@PET{revPath=xrp,node=x}, _,  _, w') ->
                if goal x
                  then Just tx
                  else aStar' . foldl' expand
                                       (s { waiting = w',
                                            visited = Set.insert x (visited s)})
                              $ let newLen = 1 + pathLen tx
                                 in map (\(l,n) -> PET{revPath=l:xrp,pathLen=newLen,node=n})
                                    (Map.toList $ graph x)
        expand s ty@PET{node=y}
          = if Set.member y $ visited s then s
            else 
              let v = pathLen ty
              in case PSQ.lookup ty (waiting s) of
                   Nothing -> link ty v s
                   Just _  -> s
        link ty v s
           = s { waiting  = PSQ.insert ty v () (waiting s) }

-- | This function computes an optimal (minimal distance) path through a graph in a best-first fashion,
-- starting from a given starting point.
aStar :: (Hashable a, Ord a, Hashable el) =>
         (a -> HashMap el a) -- ^ The graph we are searching through, given as a function from vertices
                             -- to their neighbours via labeled edges.
         -> (a -> Bool)      -- ^ The goal, specified as a boolean predicate on vertices.
         -> a                -- ^ The vertex to start searching from.
         -> Maybe ([el], a)  -- ^ An optimal path, if any path exists. This excludes the starting vertex.
aStar graph goal start
    = let mpet = runAStar graph goal start
      in fmap (\pet -> (reverse $ revPath pet, node pet)) mpet

runAStarM :: (Monad m, Hashable a, Ord a, Hashable el) =>
          (a -> m (HashMap el a))             -- adjacencies in graph
          -> (a -> m Bool)                    -- goal
          -> a                                -- starting vertex
          -> m (Maybe (PathEdgesTagged el a)) -- final state

runAStarM graph goal start = aStar' (aStarInit start)
  where aStar' s
          = case minView (waiting s) of
              Nothing            -> return Nothing
              Just (tx@PET{revPath=xrp,node=x}, _,  _, w') ->
                do g <- goal x
                   if g then return $ Just tx
                        else do ns <- graph x
                                u <- foldM expand
                                           (s { waiting = w',
                                                visited = Set.insert x (visited s)})
                                           $ let newLen = 1 + pathLen tx
                                              in map (\(lbl,n) ->
                                                       PET{revPath=lbl:xrp,pathLen=newLen,node=n})
                                                     (Map.toList ns)
                                aStar' u
        expand s ty
          = return $ if Set.member (node ty) $ visited s
                      then s
                      else
                       case PSQ.lookup ty (waiting s) of
                        Nothing -> link ty s
                        Just _  -> s
        link ty s
           = s { waiting  = PSQ.insert ty (pathLen ty) () (waiting s) }

-- | This function computes an optimal (minimal distance) path through a graph in a best-first fashion,
-- starting from a given starting point.
aStarM :: (Monad m, Hashable a, Ord a, Hashable el) =>
         (a -> m (HashMap el a)) -- ^ The graph we are searching through, given as a function from vertices
                                 -- to their neighbours.
         -> (a -> m Bool)        -- ^ The goal, specified as a boolean predicate on vertices.
         -> m a                  -- ^ The vertex to start searching from.
         -> m (Maybe ([el], a))  -- ^ An optimal path, if any path exists. This excludes the starting vertex.
aStarM graph goal start
    = do sv <- start
         mpet <- runAStarM graph goal sv
         return $ fmap (\pet -> (reverse $ revPath pet, node pet)) mpet
