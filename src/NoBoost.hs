{-# LANGUAGE DeriveDataTypeable, TupleSections #-}
module NoBoost where

-- base
import Control.Monad (guard)
import Data.Foldable (foldl')
import Data.Functor.Identity (Identity(Identity))
import Data.Maybe (catMaybes)
import Data.Typeable (Typeable)

-- hashable
import Data.Hashable (Hashable(hashWithSalt), Hashed, unhashed)

-- transformers
import Control.Monad.Trans.Class (MonadTrans(lift))
import Control.Monad.Trans.Reader (ReaderT, Reader, asks, mapReaderT)

-- unordered-containers
import qualified Data.HashMap.Strict as HashMap
import Data.HashMap.Strict (HashMap)
import qualified Data.HashSet as HashSet
import Data.HashSet (HashSet)

-- vector
import Data.Vector (Vector)
import qualified Data.Vector.Generic as Vector
import Data.Vector.Generic ((!))
import qualified Data.Vector.Unboxed as Unboxed

-- icfp2019-junkfood
import Solution
import Task

newtype BitMap = BitMap { unBitMap :: Vector (Unboxed.Vector Bool) }
  deriving (Eq, Ord, Show, Typeable)

instance Hashable BitMap where
  hashWithSalt salt (BitMap vv) = foldl' (Vector.foldl' hashWithSalt) salt vv 

data NoBoostEnv = NBEnv
  { nbeMaxX     :: Int
  , nbeMaxY     :: Int
  , nbeWalkable :: Hashed BitMap
  } deriving (Eq, Show, Typeable)

type NoBoostState = Point

moveNBState :: Delta -> NoBoostState -> ReaderT NoBoostEnv Maybe NoBoostState
moveNBState d pos = do
  let newPos = addDelta pos d
  guard $ 0 <= x newPos && 0 <= y newPos
  mx <- asks nbeMaxX
  guard $ x newPos < mx
  my <- asks nbeMaxY
  guard $ y newPos < my
  w <- asks $ (! y newPos) . (! x newPos) . unBitMap . unhashed . nbeWalkable
  guard w
  return newPos

actionNBState :: Action -> NoBoostState -> ReaderT NoBoostEnv Maybe NoBoostState
actionNBState W = moveNBState Point{x=   0,y=   1}
actionNBState A = moveNBState Point{x=(-1),y=   0}
actionNBState S = moveNBState Point{x=   0,y=(-1)}
actionNBState D = moveNBState Point{x=   1,y=   0}
actionNBState _ = const $ lift Nothing

graphNoBoost :: NoBoostState -> Reader NoBoostEnv (HashMap Action NoBoostState)
graphNoBoost pos = do
  adjList <- fmap catMaybes $ traverse gen [W,A,S,D]
  return $ HashMap.fromList adjList
 where
  gen act = mapReaderT Identity . fmap (act,) $ actionNBState act pos

heuristicNB :: HashSet Point -> NoBoostState -> Reader NoBoostEnv Int
heuristicNB goals pos = do
  mx <- asks nbeMaxX
  my <- asks nbeMaxY
  return . HashSet.foldl' min (mx + my) $ HashSet.map manhatDist goals
 where
  manhatDist dest = abs (x dest - x pos) + abs (y dest - y pos)

goalSetNB :: HashSet Point -> NoBoostState -> Bool
goalSetNB = flip HashSet.member

goalBitmapNB :: BitMap -> NoBoostState -> Bool
goalBitmapNB (BitMap vv) pos = vv ! x pos ! y pos
