PROG=${1:-$(stack path --local-install-root)/bin/solve-task}
MAX_PROC=15
MAX_CPU_SECS=360
MAX_MEM_KB=$((4 << 20))

n=0
for f in problems/*/prob-*.desc; do
	sf=${f##problems/*/prob-}
	sf=${sf%%.desc}
	out=solutions/prob-${sf}.sol
	if [ -f "$out" ]; then
		(
			tmp=$(mktemp)
			if ( ulimit -m "$MAX_MEM_KB" && ulimit -t "$MAX_CPU_SECS" && "$PROG" "$sf" ) < "$f" > "$tmp"; then
				if [ "$(stat -c %s "$tmp")" -lt "$(stat -c %s "$out")" ]; then
					mv -f "$tmp" "$out"
				else
					rm "$tmp"
				fi
			else
				rm "$tmp"
			fi
		) &
		n=$((n+1))
	else
		{
			( ulimit -m "$MAX_MEM_KB" && ulimit -t "$MAX_CPU_SECS" && "$PROG" "$sf" ) < "$f" > "$out" || rm "$out"
		} &
		n=$((n+1))
	fi
	if ! [ "$n" -le "$MAX_PROC" ]; then
		wait
		n=0
	fi
done
wait \
	&& (
		cd solutions \
			&& zip ../solutions.zip prob-*.sol
	)
