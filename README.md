# icfp2019-junkfood

Team Junkfood entry for ICFP 2019 Programming Contest

# Building the thing

You will need `stack` to build the project. It can be obtained
[here] (https://docs.haskellstack.org).

Once you have `stack` installed you need to use this incantation to be ready
to develop.

```
$ stack setup
$ stack build
```

All future builds should only require `stack build`. If you have any troubles
with the tooling, just ask on slack.

Happy Hacking!
