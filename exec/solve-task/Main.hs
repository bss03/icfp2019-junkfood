{-# LANGUAGE TupleSections #-}
module Main where

-- base
import Control.Applicative ((<|>))
import Control.Monad (guard)
import Data.Functor.Identity (runIdentity)
import Data.List (unfoldr)
import Prelude hiding (getContents)
import System.Exit (die, exitSuccess)
import System.IO (stdout)

-- bytestring
import Data.ByteString.Builder (hPutBuilder)
import Data.ByteString.Lazy (getContents)

-- hashable
import Data.Hashable (hashed, unhashed)

-- megaparsec
import Text.Megaparsec (parse)
import Text.Megaparsec.Error (errorBundlePretty)

-- transformers
import Control.Monad.Trans.Reader (ReaderT(runReaderT), mapReaderT, runReader)

-- unordered-containers
import qualified Data.HashMap.Strict as HashMap
import qualified Data.HashSet as HashSet
import Data.HashSet (HashSet)

-- our library
import Task
import Solution
import MineMap
import NoBoost
import NoWrap
import AStar

solveTask :: TaskEx -> IO Solution
solveTask = return . fmap return . steps . fromTaskEx

steps :: MinerState -> [Action]
steps = concat . unfoldr step

planVisitAnyNoBoosts :: HashSet Point -> NoBoostState -> ReaderT NoBoostEnv Maybe [Action]
planVisitAnyNoBoosts ps nbs = do
  guard (not $ HashSet.null ps)
  let
    -- distance = const . const $ pure 1
    -- hueristic = hueristicFinding ps
    isGoal = pure . goalSetNB ps
    start = pure nbs
  fmap fst . mapReaderT runIdentity
           $ aStarM graphNoBoost {-distance-} {-hueristic-} isGoal start

planVisitAnyNoWrap :: BitMap -> NoWrapState -> ReaderT NoWrapEnv Maybe [Action]
planVisitAnyNoWrap bm@(BitMap vv) nws = do
  guard (anyUnwrapped vv)
  let
    -- distance = const . const $ pure 1
    -- heuristic = const $ pure 1
    isGoal = pure . goalBitmapNW bm
    start = pure nws
  fmap fst . mapReaderT runIdentity
           $ aStarM graphNoWrap {-distance heuristic-} isGoal start

projectNoWrap :: MinerState -> (NoWrapEnv, NoWrapState)
projectNoWrap st = (nwe, nws)
 where
  ms = unhashed $ mapState st
  bs = boostState st
  nwe = NWEnv
    { nweMaxX = maximumX ms
    , nweMaxY = maximumY ms
    }
  nws = NWSt
    { nwsPosition = minerPos st
    , nwsWalkable = hashed . BitMap $ walkable ms
    , nwsFastTurns = speedTurns bs
    , nwsFastCount = speedCount bs
    , nwsDrillTurns = drillTurns bs
    , nwsDrillCount = drillCount bs
    }

step :: MinerState -> Maybe ([Action], MinerState)
step st = stepToBoosts <|> stepToUnwrapped
 where
  (nwe, nws) = projectNoWrap st
  (nbe, nbs) = (runReader $ projectNoBoost nws) nwe
  stepToBoosts = do
    let
      desiredBoosts = HashMap.keysSet . HashMap.filter (`elem` [Speed, Drill])
                                      . boostLocs . unhashed $ mapState st
    plan <- runReaderT (planVisitAnyNoBoosts desiredBoosts nbs) nbe
    newSt <- actionsState plan st
    return (plan, newSt)
  stepToUnwrapped = do
    plan <- runReaderT (planVisitAnyNoWrap nw nws) nwe
    newSt <- actionsState plan st
    return (plan, newSt)
   where
    nw = BitMap . needsWrap . unhashed $ mapState st

main :: IO ()
main = do
  taskData <- getContents
  tsk <- case parse taskParser "(stdin)" taskData of
   Left eb -> die $ errorBundlePretty eb
   Right t -> return t
  let taskEx = extendTask tsk
  solution <- solveTask taskEx
  hPutBuilder stdout $ solutionBuilder solution
  putStrLn ""
  exitSuccess
