module Main where

-- base
import Prelude hiding (getContents)
import System.Exit (die, exitSuccess)
import Data.Void (Void)

-- bytestring
import qualified Data.ByteString.Lazy as Lazy
import Data.ByteString.Lazy (getContents)

-- megaparsec
import Text.Megaparsec (Parsec, parse)
import Text.Megaparsec.Error (errorBundlePretty)

-- local libraries
import Puzzle

solvePuzzle :: Puzzle -> IO PuzzleSolution
solvePuzzle _p = undefined

main :: IO ()
main = do
  puzzleData <- getContents
  case parse (puzzleParser :: Parsec Void Lazy.ByteString Puzzle) "(stdin)" puzzleData of
    Left e -> do
      putStrLn $ errorBundlePretty e
      die "What kind of puzzle are you trying to make me solve?!"
    Right puzz -> do
      putStrLn $ show puzz
      let solution = mkSolution puzz
      putStrLn $ show (vertexes solution)
      --hPutBuilder stdout $ puzzleBuilder solution
      --putStrLn ""
      exitSuccess
