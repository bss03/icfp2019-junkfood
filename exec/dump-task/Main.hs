module Main where

-- base
import Prelude hiding (getContents)
import System.Exit (die, exitSuccess)

-- bytestring
import Data.ByteString.Lazy (getContents)

-- megaparsec
import Text.Megaparsec (parse)
import Text.Megaparsec.Error (errorBundlePretty)

-- our library
import Task

main :: IO ()
main = do
  taskData <- getContents
  case parse taskParser "(stdin)" taskData of
   Left eb -> die $ errorBundlePretty eb
   Right tsk -> do
    print $ extendTask tsk
    exitSuccess
