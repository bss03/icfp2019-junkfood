module Main where

-- base
import System.IO (stdout)

-- bytestring
import Data.ByteString.Builder (hPutBuilder)

-- our library
import Solution

main :: IO ()
main = do
  solution <- fmap read getContents
  hPutBuilder stdout $ solutionBuilder solution
  putStrLn ""
